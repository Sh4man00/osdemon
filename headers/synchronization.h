#ifndef SYNCHRONIZATION_H
#define SYNCHRONIZATION_H

void synchronize(const char* srcPath, const char* dstPath, size_t sizeLimit, int recursionFlag);

#endif