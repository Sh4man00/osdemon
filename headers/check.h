#ifndef CHECK_H
#define CHECK_H

int isNumber(const char* number);
int directoryValidation(const char* pathToDirectory);

#endif