#ifndef FOPERATIONS_H
#define FOPERATIONS_H

#include <stdio.h>

void useMmap(const char* srcPath, const char* dstPath);
ssize_t write_all (int fd, const void* buffer, size_t count);
void readWrite(const char* srcPath, const char* dstPath);
void removeDirectory(char* dirPath);

#endif