CC = gcc
CFLAGS = -g -Wall -Iheaders -o
sources := $(wildcard sources/*.c)

all: syncDaemon

syncDaemon: $(sources)
	$(CC) $(sources) $(CFLAGS) syncDaemon

.PHONY: clean
clean:
	rm -f syncDaemon










