#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "check.h"
#include "synchronization.h"

void signalHandler(int signum){

    if(signum == SIGUSR1){
        syslog(LOG_INFO, "You are waking the daemon with SIGUSR1 (-%d)", SIGUSR1);
    }

}

int main(int argc, char* argv[]) {

    size_t sizeLimit = 2048; // default: 2kB
    unsigned int sleepTime = 300; //5min
    int recursionFlag = 0;
    int param, pathCount;
    const char* srcPath;
    const char* dstPath;
    char* timeChar = NULL;
    char* sizeChar = NULL;
    opterr = 0;

    while((param = getopt(argc, argv, "Rt:s:")) != -1){
        switch(param) {
            case 'R':
                recursionFlag = 1;
                break;

            case 't':
                if(isNumber(optarg) == 1){
                    timeChar = optarg;
                    sleepTime = atoi(timeChar) * 60;
                } else { 
                    fprintf(stderr, "ERROR: Option: '-%c' has invalid argument.\n", param);  
                    exit(EXIT_FAILURE);
                }
                break;

            case 's':
                if(isNumber(optarg) == 1){
                    sizeChar = optarg;
                    sizeLimit = atoi(sizeChar) * 1024 * 1024;
                } else { 
                    fprintf(stderr, "ERROR: Option: '-%c' has invalid argument.\n", param);
                    exit(EXIT_FAILURE);
                }
                break;
                
            case '?':
                fprintf(stderr, "ERROR: Unknown option: '-%c'.", optopt);
                exit(EXIT_FAILURE);
                
            default:
                fprintf(stderr, "ERROR: Can't parse arguments.");
                exit(EXIT_FAILURE);
        }
    }

    //obliczenie libczy argumentow innych niz parametry opcjonalne
    pathCount = argc - optind;

    if(pathCount == 2){
        srcPath = argv[optind];
        dstPath = argv[optind+1];
        directoryValidation(srcPath);
        directoryValidation(dstPath);
    } else if (pathCount < 2){
        fprintf(stderr, "ERROR: Path argument is missing.");
    } else if (pathCount > 2){
        fprintf(stderr, "ERROR: Too many path arguments.");
    }

    pid_t pid, sid;

    //fork, jesli operacja sie powiedzie to wychodzi z procesu rodzica
    pid = fork();
    if(pid < 0) {
        fprintf(stderr, "ERROR: Couldn't fork proccess\n.");
        exit(EXIT_FAILURE);
    }

    if(pid > 0) {
        exit(EXIT_SUCCESS);
    }

    //moga byc ustawione dowolne uprawnienia
    umask(0);

    openlog("syncDaemonLog", LOG_PID, LOG_LOCAL0);
    syslog(LOG_INFO, "INFO: Initialized syncDaemonLog.");

    sid = setsid();
    if(sid < 0) {
        syslog(LOG_ERR, "ERROR: Couldn't set sid.");
        closelog();
        exit(EXIT_FAILURE);
    }

    
    if(chdir("/") < 0) {
        syslog(LOG_ERR, "ERROR: Working directory isn't root '/'.");
        closelog();
        exit(EXIT_FAILURE);
    }

    signal(SIGUSR1, signalHandler);

    //zamkniete deskryptory
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    syslog(LOG_INFO, "You created a daemon. Beware.");
    //petla demona
    while(1) {
        syslog(LOG_INFO, "Daemon is in sleeping mode for %d minutes", sleepTime/60);
        sleep(sleepTime);
        syslog(LOG_INFO, "Daemon woke up");
        syslog(LOG_INFO, "Daemon is synchronizing files...");
        synchronize(srcPath, dstPath, sizeLimit, recursionFlag);
    }
    
    closelog();
    exit(EXIT_SUCCESS);
    
}