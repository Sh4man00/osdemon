#include "foperations.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <assert.h>
#include <syslog.h>
#include <string.h>
#include <fcntl.h>

void useMmap(const char* srcPath, const char* dstPath){

    struct stat fileStats;

    int srcFd = open(srcPath, O_RDONLY);
    if(srcFd < 0){
        syslog(LOG_ERR, "ERROR: %s: %s.", srcPath, strerror(errno));
    }

    int dstFd = open(dstPath, O_RDWR | O_CREAT);
    if(dstFd < 0){
        syslog(LOG_ERR, "ERROR: %s: %s.", dstPath, strerror(errno));
    }

    stat(srcPath, &fileStats);
    size_t fileSize = fileStats.st_size;
    char* srcMap = mmap(NULL, fileSize, PROT_READ, MAP_PRIVATE, srcFd, 0);

    ftruncate(dstFd, fileSize);
    char* dstMap = mmap(NULL, fileSize, PROT_READ | PROT_WRITE, MAP_SHARED, dstFd, 0);
    
    memcpy(dstMap, srcMap, fileSize);
    munmap(srcMap, fileSize);
    munmap(dstMap, fileSize);

    close(srcFd);
    close(dstFd);

}

/* Write all of COUNT bytes from BUFFER to file descriptor FD. 
   Returns -1 on error, or the number of bytes written.
   zrodlo: ALP - Reading and Writing Data  */ 

ssize_t write_all (int fd, const void* buffer, size_t count) {
    size_t left_to_write = count; 
    while (left_to_write > 0) {
        size_t written = write (fd, buffer, count); 
        if (written == -1) 
            /* An error occurred; bail.  */ 
            return -1; 
        else 
            /* Keep count of how much more we need to write.  */ 
            left_to_write -= written; 
    } 
    /* We should have written no more than COUNT bytes!   */ 
    assert (left_to_write == 0); 
    /* The number of bytes written is exactly COUNT.  */ 
    return count;
     
} 

void readWrite(const char* srcPath, const char* dstPath){

    int fdDst, fdSrc;
    //szesnastkowo
    unsigned char buffer[16];
    size_t bytes;

    fdSrc = open(srcPath, O_RDONLY);
    if(fdSrc < 0){
        syslog(LOG_ERR, "ERROR: %s: %s.", srcPath, strerror(errno));
    }

    fdDst = open(dstPath, O_CREAT | O_WRONLY);

    if(fdDst < 0){
        syslog(LOG_ERR, "ERROR: %s: %s.", dstPath, strerror(errno));
    }

    do {
        bytes = read(fdSrc, buffer, sizeof(buffer));
        if(write_all(fdDst, buffer, bytes) < 0){
            syslog(LOG_ERR, "ERROR: Error occured during writing to file");
        }
    } while(bytes == sizeof(buffer));

   close(fdSrc);
   close(fdDst);
  
}

void removeDirectory(char* dirPath) {

    DIR* directory = opendir(dirPath);
    char dirFilePath[4096];
    struct dirent* dirFile;
    struct stat dirStat;
    if(directory){

        while( (dirFile = readdir(directory)) ){
                sprintf(dirFilePath, "%s/%s", dirPath, dirFile->d_name);
                lstat(dirFilePath, &dirStat);
                if (S_ISREG(dirStat.st_mode)) {
                    syslog(LOG_INFO, "Daemon is deleting file: %s", dirFile->d_name);
                    remove(dirFilePath);
                } else if (S_ISDIR(dirStat.st_mode) && strcmp(dirFile->d_name, ".") != 0 && strcmp(dirFile->d_name, "..") != 0){
                    removeDirectory(dirFilePath);
                } else {
                    continue;
                }
        }

    closedir(directory);
    syslog(LOG_INFO, "Daemon is deleting directory: %s", dirPath);
    remove(dirPath);

    } else {

        syslog(LOG_ERR, "ERROR: Daemon doesn't know what to do. %s", strerror(errno));
        closelog();
        exit(EXIT_FAILURE);
    }

    return;
}