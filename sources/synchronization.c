#include "check.h"
#include "foperations.h"
#include <stdlib.h>
#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <dirent.h>
#include <utime.h>
#include <sys/stat.h>
#include <sys/types.h>

void synchronize(const char* srcPath, const char* dstPath, size_t sizeLimit, int recursionFlag){

    DIR* source;
    DIR* destination;
    source = opendir(srcPath);
    destination = opendir(dstPath);
    struct dirent* srcFile;
    struct dirent* dstFile;
    struct stat srcStat, dstStat;
    char srcFilePath[4096];
    char dstFilePath[4096];
    struct utimbuf srcTimes;

    //sprawdzenie czy nadal istnieja ktalogi
    
    if(source && destination){
        while( (srcFile = readdir(source)) ){
            sprintf(srcFilePath, "%s/%s", srcPath, srcFile->d_name);
            sprintf(dstFilePath, "%s/%s", dstPath, srcFile->d_name);
            lstat(srcFilePath, &srcStat);
            lstat(dstFilePath, &dstStat);
            if(S_ISDIR(srcStat.st_mode) && recursionFlag == 1 && strcmp(srcFile->d_name, ".") != 0 && strcmp(srcFile->d_name, "..") != 0){
                if (lstat(dstFilePath, &dstStat) == -1) {
                    mkdir(dstFilePath, srcStat.st_mode);
                }

                synchronize(srcFilePath, dstFilePath, sizeLimit, recursionFlag);

                if(srcStat.st_mtime == dstStat.st_mtime){
                    continue;
                } else {
                    srcTimes.actime = srcStat.st_atime;
                    srcTimes.modtime = srcStat.st_mtime;
                    utime(dstFilePath, &srcTimes);
                    chmod(dstFilePath, srcStat.st_mode);
                }

            } else if (S_ISREG(srcStat.st_mode)){
                //synchronizacja
                if(srcStat.st_mtime == dstStat.st_mtime){
                    continue;
                } else {
                    
                    if(srcStat.st_size < sizeLimit){
                        syslog(LOG_INFO, "Daemon is writing file: %s using write", srcFile->d_name);
                        readWrite(srcFilePath, dstFilePath);
                    } else {
                        syslog(LOG_INFO, "Daemon is writing file: %s using mmap", srcFile->d_name);
                        useMmap(srcFilePath, dstFilePath);
                    }
                    
                    srcTimes.actime = srcStat.st_atime;
                    srcTimes.modtime = srcStat.st_mtime;
                    utime(dstFilePath, &srcTimes);
                    chmod(dstFilePath, srcStat.st_mode);
                }

            }

        }

        //usuniecie plikow nadmiarowych z folderu docelowego
        while( (dstFile = readdir(destination)) ){
            sprintf(dstFilePath, "%s/%s", dstPath, dstFile->d_name);
            sprintf(srcFilePath, "%s/%s", srcPath, dstFile->d_name);
            lstat(dstFilePath, &dstStat);

            if (S_ISREG(dstStat.st_mode)){
                if(lstat(srcFilePath, &srcStat) == -1){
                    syslog(LOG_INFO, "Daemon is deleting file: %s", dstFile->d_name);
                    remove(dstFilePath);
                }
            } else if((S_ISDIR(dstStat.st_mode) && recursionFlag == 1)) {

                if(lstat(srcFilePath, &srcStat) == -1){
                    removeDirectory(dstFilePath);
                }

            } else {
                    continue;
            }
        }
             
        closedir(source);
        closedir(destination);

    } else {

        syslog(LOG_ERR, "ERROR: Daemon doesn't know what to do. %s", strerror(errno));
        closelog();
        exit(EXIT_FAILURE);
    }

    return;

}