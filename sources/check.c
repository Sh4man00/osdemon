#include <dirent.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "check.h"

int isNumber(const char* number){
    
    int i;

    for(i = 0; number[i] != '\0'; i++){
        if(!isdigit(number[i])){
            return 0;
        }
    }

    return 1;

}

int directoryValidation(const char* pathToDirectory){

    DIR* directory;
    directory = opendir(pathToDirectory);

    if(directory) {
        closedir(directory);
        return 1;
    } else {
        perror(pathToDirectory);
        exit(EXIT_FAILURE);
    }

}